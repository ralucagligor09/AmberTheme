$(document).ready(function () {

    // Start Header Actions
    try {
        //Sticky Header
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll >= 50) {
                $(".header").addClass("stickyheader");
            } else {
                $(".header").removeClass("stickyheader");
            }
        });

        //Mobile Menu
        $(".show-menu").click(function () {
            $(".second-navbar").slideToggle();
            $(this).toggleClass("button-animation");
        });

        $(".second-navbar a").click(function () {
            $(".show-menu").removeClass("button-animation");
            $(".second-navbar").hide('400');
        });

        //PopUp Section
        $(".first-button").click(function () {
            $(".popup-section").slideToggle("fast");
            $("body").toggleClass("trigger");
        });

        $(".popupbutton").click(function () {
            $("body").removeClass('trigger');
            $(".popup-section").hide('400');
        });

        //Second PopUp Section
        $(".second-button").click(function () {
            $(".second-popup-section").slideToggle("fast");
            $("body").toggleClass("trigger");
        });

        $(".second-popupbutton").click(function () {
            $("body").removeClass('trigger');
            $(".second-popup-section").hide('400');
        });

        //Scroll to top button
        $(window).scroll(function () {
            if ($(this).scrollTop() > 200) {
                $('.scrollToTop').fadeIn();
            } else {
                $('.scrollToTop').fadeOut();
            }
        });
        $('.scrollToTop').click(function(){
            $('html, body').animate({scrollTop : 0},600);
            return false;
        });
    } catch (e){}
    //End of Header Action

    // Main Slider Start

    try {
        $('.main-slider').slick({
            dots: true,
            animating: true,
            infinite: true,
            speed: 600,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 12000,
            responsive: [
                {
                    breakpoint: 600,
                    settings: {
                        dots: false
                    }
                }
            ]
        });
    } catch (e){}
    //End of Main Slider

    // Testimonials Slider Start
    try {
        $('.testimonial-slider').slick({
            dots: true,
            animating: true,
            infinite: true,
            speed: 600,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 12000,
            responsive: [
                {
                    breakpoint: 600,
                    settings: {
                        dots: false
                    }
                }
            ]
        });
    } catch (e){}
    //End of Testimonials Slider

    // Blog-Masonry Show-more-posts toggle Start

    $('.show-more-posts').click(function () {
$('.show-more-toggle').toggle();

    })
});